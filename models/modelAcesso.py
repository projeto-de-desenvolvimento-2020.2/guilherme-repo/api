from banco import db
from datetime import datetime

class Acesso(db.Model):
    __tablename__ = 'acessos'
    acessoId = db.Column(db.Integer, autoincrement=True, primary_key=True)
    tipoRegistro = db.Column(db.Integer, nullable=False)
    registroId = db.Column(db.Integer,  nullable=False)
    qtdAcesso = db.Column(db.Integer, nullable=False)
    

    def to_json(self):
        json_Acessos = {
            'acessoId': self.acessoId,
            'tipoRegistro': self.tipoRegistro,
            'registroId': self.registroId,
            'qtdAcesso': self.qtdAcesso
        }
        return json_Acessos

    @staticmethod
    def from_json(json_Acessos):
        tipoRegistro = json_Acessos.get('tipoRegistro')
        registroId = json_Acessos.get('registroId')
        qtdAcesso = json_Acessos.get('qtdAcesso')
        return Acesso(tipoRegistro=tipoRegistro, registroId=registroId, qtdAcesso=qtdAcesso)