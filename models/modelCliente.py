
from sqlalchemy.orm import backref
from banco import db
import hashlib
from config import config
from datetime import datetime
from models.modelProjeto import Projeto

class Cliente(db.Model):
  __tablename__ = 'clientes'
  clienteId = db.Column(db.Integer, autoincrement=True, primary_key=True)
  nome = db.Column(db.String(100), nullable=False)
  email = db.Column(db.String(100), unique=True, nullable=False)
  cpfCnpj = db.Column(db.String(14), nullable=False)
  cep = db.Column(db.String(8), nullable=False)
  logradouro = db.Column(db.String(150), nullable=False)
  cidade = db.Column(db.String(100), nullable=False)
  bairro = db.Column(db.String(100), nullable=False)
  numero = db.Column(db.String(10), nullable=False)
  telefone = db.Column(db.String(13), nullable=False)
  senha = db.Column(db.String(32), nullable=False)
  tipoCliente = db.Column(db.Integer, nullable=False)
  urlProfissional = db.Column(db.String(300), nullable=False)
  data_cad = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

  # comentarios = db.relationship('comentarios')
  # projeto = db.relationship('Projeto', backref='projeto')

  def to_json(self):
    json_clientes = {
      'clienteId': self.clienteId,
      'email': self.email,
      'nome': self.nome,
      'cpfCnpj': self.cpfCnpj,
      'cep': self.cep,
      'logradouro': self.logradouro,
      'cidade': self.cidade,
      'bairro': self.bairro,
      'numero': self.numero,
      'telefone': self.telefone,
      'tipoCliente': self.tipoCliente,
      'urlProfissional': self.urlProfissional
    }
    return json_clientes

  @staticmethod
  def cliente_projetos():
      return Cliente.query.join(Projeto, Projeto.clienteId==Cliente.clienteId)\
        .filter(Projeto.clienteId == Cliente.clienteId).all()

  @staticmethod
  def from_json(json_clientes):
    email = json_clientes.get('email')
    nome = json_clientes.get('nome')
    cpfCnpj = json_clientes.get('cpfCnpj')
    cep = json_clientes.get('cep')
    logradouro = json_clientes.get('logradouro')
    cidade = json_clientes.get('cidade')
    bairro = json_clientes.get('bairro')
    numero = json_clientes.get('numero')
    telefone = json_clientes.get('telefone')
    senha = json_clientes.get('senha') + config.SALT
    senha_md5 = hashlib.md5(senha.encode()).hexdigest()
    tipoCliente = json_clientes.get('tipoCliente')
    urlProfissional = json_clientes.get('urlProfissional')
    return Cliente(email=email, nome=nome, cpfCnpj=cpfCnpj, cep=cep, logradouro=logradouro, cidade=cidade, bairro=bairro, numero=numero, telefone=telefone, senha=senha_md5, tipoCliente=tipoCliente, urlProfissional=urlProfissional)
