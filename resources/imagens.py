from flask import Blueprint, jsonify, request
from banco import db
from flask_cors import CORS, cross_origin
from models.modelImagem import Imagem
from flask_jwt_extended import jwt_required

imagens = Blueprint('imagens', __name__)


@imagens.route('/imagens')
@cross_origin()
def listagemImagens():
    imagens = Imagem.query.all()
    return jsonify([imagem.to_json() for imagem in imagens])
    

@imagens.route('/imagens', methods=['POST'])
# Quando colocar o login descomentar essa linha
# @jwt_required
@cross_origin()
def cadastroImagens():
    imagem = Imagem.from_json(request.json)
    db.session.add(imagem)
    db.session.commit()
    return jsonify(imagem.to_json()), 201


@imagens.route('/imagens/<int:imagemId>', methods=['PUT'])
# Quando colocar o login descomentar essa linha
# @jwt_required
@cross_origin()
def alteracaoImagem(imagemId):
    imagem = Imagem.query.get_or_404(imagemId)
    imagem.tipoRegistro = request.json['tipoRegistro']
    imagem.registroId = request.json['registroId']
    imagem.urlImagem = request.json['urlImagem']   
    db.session.add(imagem)
    db.session.commit()
    return jsonify(imagem.to_json()), 200   
    

@imagens.route('/imagens/<int:imagemId>')
@cross_origin()
def getByIdImagens(imagemId):
    imagem = Imagem.query.get_or_404(imagemId)
    return jsonify(imagem.to_json()), 200   

@imagens.route('/imagens/profissional/<int:registroId>')
@cross_origin()
def listagemImagemProfissional(registroId):
    imagens = Imagem.query.filter(Imagem.registroId == registroId and Imagem.tipoRegistro == 0).all()

    return jsonify([Imagem.to_json() for Imagem in imagens])

@imagens.route('/imagens/loja/<int:registroId>')
@cross_origin()
def listagemImagemLoja(registroId):
    imagens = Imagem.query.filter(Imagem.registroId == registroId and Imagem.tipoRegistro == 1).all()

    return jsonify([Imagem.to_json() for Imagem in imagens])

@imagens.route('/imagens/projeto/<int:registroId>')
@cross_origin()
def listagemImagemProjeto(registroId):
    imagens = Imagem.query.filter(Imagem.registroId == registroId and Imagem.tipoRegistro == 2).all()

    return jsonify([Imagem.to_json() for Imagem in imagens])

@imagens.route('/imagens/produto/<int:registroId>')
@cross_origin()
def listagemImagemProduto(registroId):
    imagens = Imagem.query.filter(Imagem.registroId == registroId and Imagem.tipoRegistro == 3).all()

    return jsonify([Imagem.to_json() for Imagem in imagens])


@imagens.route('/imagens/pesq/<img>')
# Quando colocar o login descomentar essa linha
# @jwt_required
@cross_origin()
def pesquisaCliente(img):
    imagens = Imagem.query.order_by(Imagem.urlImagem).filter(Imagem.urlImagem.like(f'%{img}%')).all()
    if len(imagens):
        return jsonify([imagem.to_json() for imagem in imagens]) 
    else:
        return jsonify({'id': 0, 'message': 'Nenhuma Imagem encontrado'}), 400 


# @viagens.route('/viagens/estatisticas')
# def maior():
#     totalViagens = Viagem.query.count()
#     m = Viagem.query.order_by(Viagem.valor.desc()).limit(1).all()
#     mn = Viagem.query.order_by(Viagem.valor.asc()).limit(1).all()
#     maior = {
#         'id': m[0].idViagens,
#         'valor': m[0].valor,
#         'tipoViagem': m[0].tipoViagem
#         }
#     menor = {
#         'id': mn[0].idViagens,
#         'valor': mn[0].valor,
#         'tipoViagem': mn[0].tipoViagem
#         }
#     lista = []
#     lista.append({'totalViagens': totalViagens, 'maior': maior, 'menor': menor})
    
#     return jsonify(lista), 201


@imagens.route('/imagens/<int:imagemId>', methods=['DELETE'])
# Quando colocar o login descomentar essa linha
# @jwt_required
@cross_origin()
def exclui(imagemId):
    imagens = Imagem.query.filter_by(imagemId=imagemId).delete()
    if imagens:
        # Excursao.query.filter_by(usuarioId=usuarioId).delete()
        db.session.commit()
        return jsonify({'id': imagemId, 'message': 'Imagem excluída com sucesso'}), 200
    else:
      return jsonify({'id': 0, 'message': 'Imagem Não encontrado'}), 404      

@imagens.errorhandler(404)
def id_invalido(error):
    return jsonify({'id': 0, 'message': 'Imagem Não encontrado'}), 404  