from flask import Blueprint, jsonify, request
from banco import db
from flask_cors import CORS, cross_origin
from models.modelAcesso import Acesso
from flask_jwt_extended import jwt_required

acessos = Blueprint('acessos', __name__)


@acessos.route('/acessos')
@cross_origin()
def listagemAcessos():
    acessos = Acesso.query.all()
    return jsonify([acesso.to_json() for acesso in acessos])
    

@acessos.route('/acessos', methods=['POST'])
# Quando colocar o login descomentar essa linha
# @jwt_required
@cross_origin()
def cadastroAcessos():
    acesso = Acesso.from_json(request.json)
    db.session.add(acesso)
    db.session.commit()
    return jsonify(acesso.to_json()), 201


@acessos.route('/acessos/<int:acessoId>', methods=['PUT'])
# Quando colocar o login descomentar essa linha
# @jwt_required
@cross_origin()
def alteracaoAcesso(acessoId):
    # acesso = db.session.query(Acesso.registroId).filter(Acesso.registroId == registroId and Acesso.tipoRegistro == 0)
    acesso = Acesso.query.get_or_404(acessoId)
    print(acesso)
    acesso.qtdAcesso += 1 
    db.session.add(acesso)
    db.session.commit()
    return jsonify(acesso.to_json()), 200   
    


@acessos.route('/acessos/<int:acessoId>', methods=['DELETE'])
# Quando colocar o login descomentar essa linha
# @jwt_required
@cross_origin()
def exclui(acessoId):
    acessos = Acesso.query.filter_by(acessoId=acessoId).delete()
    if acessos:
        db.session.commit()
        return jsonify({'id': acessoId, 'message': 'Acesso excluído com sucesso'}), 200
    else:
      return jsonify({'id': 0, 'message': 'Acesso Não encontrado'}), 404      

@acessos.errorhandler(404)
def id_invalido(error):
    return jsonify({'id': 0, 'message': 'Acesso Não encontrado'}), 404  