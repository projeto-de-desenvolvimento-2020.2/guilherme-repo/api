from flask import Blueprint, jsonify, request
from banco import db
from flask_cors import CORS, cross_origin
from models.modelUsuario import Usuario
from flask_jwt_extended import jwt_required

usuarios = Blueprint('usuarios', __name__)


@usuarios.route('/usuarios')
@cross_origin()
def listagemUsuarios():
    usuarios = Usuario.query.order_by(Usuario.nome).all()
    return jsonify([usuario.to_json() for usuario in usuarios])
    

@usuarios.route('/usuarios', methods=['POST'])
# Quando colocar o login descomentar essa linha
# @jwt_required
@cross_origin()
def cadastroUsuarios():
    usuario = Usuario.from_json(request.json)
    db.session.add(usuario)
    db.session.commit()
    return jsonify(usuario.to_json()), 201


@usuarios.route('/usuarios/<int:usuarioId>', methods=['PUT'])
# Quando colocar o login descomentar essa linha
# @jwt_required
@cross_origin()
def alteracaoUsuario(usuarioId):
    usuario = Usuario.query.get_or_404(usuarioId)
    usuario.email = request.json['email']
    usuario.nome = request.json['nome']
    usuario.cpfCnpj = request.json['cpfCnpj']  
    usuario.cep = request.json['cep']  
    usuario.logradouro = request.json['logradouro']  
    usuario.cidade = request.json['cidade']  
    usuario.bairro = request.json['bairro']  
    usuario.numero = request.json['numero']  
    usuario.telefone = request.json['telefone']  
    usuario.senha = request.json['senha']  
    db.session.add(usuario)
    db.session.commit()
    return jsonify(usuario.to_json()), 200   
    

@usuarios.route('/usuarios/<int:usuarioId>')
@cross_origin()
def getByIdUsuarios(usuarioId):
    usuario = Usuario.query.get_or_404(usuarioId)
    return jsonify(usuario.to_json()), 200   


@usuarios.route('/usuarios/pesq/<nome>')
# Quando colocar o login descomentar essa linha
# @jwt_required
@cross_origin()
def pesquisaUsuario(nome):
    usuarios = Usuario.query.order_by(Usuario.nome).filter(Usuario.nome.like(f'%{nome}%')).all()
    if len(usuarios):
        return jsonify([usuario.to_json() for usuario in usuarios]) 
    else:
        return jsonify({'id': 0, 'message': 'Nenhum usuario encontrado'}), 400 


# @viagens.route('/viagens/estatisticas')
# def maior():
#     totalViagens = Viagem.query.count()
#     m = Viagem.query.order_by(Viagem.valor.desc()).limit(1).all()
#     mn = Viagem.query.order_by(Viagem.valor.asc()).limit(1).all()
#     maior = {
#         'id': m[0].idViagens,
#         'valor': m[0].valor,
#         'tipoViagem': m[0].tipoViagem
#         }
#     menor = {
#         'id': mn[0].idViagens,
#         'valor': mn[0].valor,
#         'tipoViagem': mn[0].tipoViagem
#         }
#     lista = []
#     lista.append({'totalViagens': totalViagens, 'maior': maior, 'menor': menor})
    
#     return jsonify(lista), 201


@usuarios.route('/usuarios/<int:usuarioId>', methods=['DELETE'])
# Quando colocar o login descomentar essa linha
# @jwt_required
@cross_origin()
def exclui(usuarioId):
    usuarios = Usuario.query.filter_by(usuarioId=usuarioId).delete()
    if usuarios:
        # Excursao.query.filter_by(usuarioId=usuarioId).delete()
        db.session.commit()
        return jsonify({'id': usuarioId, 'message': 'Usuario excluído com sucesso'}), 200
    else:
      return jsonify({'id': 0, 'message': 'Usuario Não encontrado'}), 404      

@usuarios.errorhandler(404)
def id_invalido(error):
    return jsonify({'id': 0, 'message': 'Usuario Não encontrado'}), 404  