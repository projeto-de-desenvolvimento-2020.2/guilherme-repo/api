# BACK END DA APLICAÇÃO CONSTRUOBRE

## Apropriar do Projeto

git clone https://gitlab.com/projeto-de-desenvolvimento-2020.2/guilherme-repo/api.git

## Dependências para Instalar
* flask_sqlalchemy // comando para instalar: pip install Flask-SQLAlchemy
* SQLAlchemy // comando para instalação: pip install SQLAlchemy
* Flask // comando para instalação: pip install Flask
* flask_cors  // comando para instalação: pip install Flask-Cors
* PyMySQL  // comando para instalação: pip install PyMySQL
* blueprint  // comando para instalação: pip install blueprint
* flask-jwt-extended  // comando para instalação: pip install flask-jwt-extended

## instalar o banco de dados Mysql e o servidor Apache:
* recomenda-se instalar o XAMPP / https://www.apachefriends.org/pt_br/index.html

* após a instalação do XAMPP criar uma database com o nome:
* obra_noite

## Rodar a Aplicação
* python app.py

## Para criar as tabelas no Mysql:
* executar http://127.0.0.1:5000/ no navegador

## Deploy
* http://construobre.pythonanywhere.com/

# Routes Principais
## clientes.route
* Métodos e Rotas:
* get:  /clientes
* post:  /clientes
* put:  /clientes/<int:clienteId>
* delete:  /clientes/<int:clienteId>

## usuarios.route
* Métodos e Rotas:
* get:  /usuarios
* post:  /usuarios
* put:  /usuarios/<int:usuarioId>
* delete:  /usuarios/<int:usuarioId>
  
##  login.routes logins.routes
* Métodos e Rotas:
* get:  /logins
* post:  /logins

### logar e deslogar no sistema chamar o método e rota:
* post /login
* get  /logout 

## projetos.route
* Métodos e Rotas:
* get:  /projetos
* post:  /projetos
* put:  /projetos/<int:projetoId>
* delete:  /projetos/<int:projetoId>

## imagens.route
* Métodos e Rotas:
* get:  /imagens
* post:  /imagens
* put:  /imagens/<int:imagemId>
* delete:  /imagens/<int:imagemId>